import glob
import os
import time
import tkinter as tk
from tkinter import *
from PIL import Image as Img
from PIL import ImageTk

import cv2


def get_frames(cap, time_video, fps):
    if not os.path.isdir("saved_frames"):
        os.mkdir("saved_frames")

    if not cap.isOpened():
        raise IOError("Cannot open webcam")

    counter = 1
    video_time = 1
    while True:
        # читаем кадры с записи или камеры в frame
        _, frame = cap.read()
        crop_img = frame
        cv2.imwrite("./saved_frames/frame{0}.jpg".format(counter), crop_img)
        time.sleep(fps)
        counter = counter + 1
        video_time = video_time + 1
        if video_time > time_video:
            break
    print("Successfully saved")


def show_frame():
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Img.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame)


def save_frames(cap):
    root = Tk()

    root['bg'] = '#fafafa'
    root.title('Снять кадры')
    root.wm_attributes('-alpha', 1)
    root.geometry('300x300')

    root.resizable(height=False, width=False)
    frame = Frame(root, bg='white')
    frame.place(relwidth=1, relheight=1)
    title1 = Label(frame, text='Период съемки/сек', bg='white', font=40)
    title1.pack()
    input_time = Entry(frame, bg='white')
    input_time.pack()
    title2 = Label(frame, text='Частота кадров/сек', bg='white', font=40)
    title2.pack()
    input_frames = Entry(frame, bg='white')
    input_frames.pack()
    btn = Button(frame, text='Снять кадры', bg='turquoise',
                 command=lambda: get_frames(cap, int(input_time.get()), int(input_frames.get()))).place(x=100, y=100)
    root.mainloop()


def create_video():
    out = cv2.VideoWriter('video.mp4', cv2.VideoWriter_fourcc(*'XVID'), 5, (640, 480))
    for filename in glob.glob('./saved_frames/*.jpg'):
        frame_for_video = cv2.imread(filename)
        out.write(frame_for_video)

    print('video created')


def create_gif():
    frames = []
    for filename in glob.glob('./saved_frames/*.jpg'):
        frame_for_gif = Img.open(filename)
        frames.append(frame_for_gif)

    frames[0].save('gifka.gif', save_all=True, append_images=frames[0:], optimize=True, duration=200, loop=0)
    print('gif created')


if __name__ == "__main__":
    cap = cv2.VideoCapture(0)

    root = Tk()

    root['bg'] = '#fafafa'
    root.title('Конвертер с веб камеры')
    root.wm_attributes('-alpha', 1)
    root.geometry('900x500')

    imageFrame = tk.Frame(root, width=600, height=500)
    imageFrame.grid(row=0, column=0, padx=10, pady=2)

    lmain = tk.Label(imageFrame)
    lmain.grid(row=0, column=0)

    sliderFrame = tk.Frame(root, width=600, height=100)
    sliderFrame.grid(row=600, column=0, padx=10, pady=2)

    show_frame()

    root.resizable(height=False, width=False)

    frame = Frame(root, bg='pink')
    frame.place(relx=0.7, relwidth=0.3, relheight=1)
    btn1 = Button(frame, text='Снять кадры', height=3, width=14, bg='turquoise',
                  command=lambda: save_frames(cap)).place(x=100, y=60)
    btn2 = Button(frame, text='Сделать гифку', height=3, width=14, bg='turquoise', command=create_gif) \
        .place(x=100, y=150)
    btn3 = Button(frame, text='Сделать видео', height=3, width=14, bg='turquoise', command=create_video) \
        .place(x=100, y=240)

    root.mainloop()
